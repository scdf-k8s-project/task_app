FROM springcloud/openjdk
COPY target/task-demo-metrics-0.0.1-SNAPSHOT.jar /maven/
VOLUME ["/tmp"]
ENTRYPOINT ["java","-jar","/maven/task-demo-metrics-0.0.1-SNAPSHOT.jar"]
